<html>
    <head>
        <title>CIS 322 REST-api demo: Laptop list</title>
    </head>

    <body>
        <h1>List of laptops</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/');
            $obj = json_decode($json);
            echo json_encode($json, JSON_PRETTY_PRINT);
            ?>
        </ul>
    </body>
</html>
